<?php

class LogoutController extends Controller {

    function index() {
        session_start();
        $_SESSION = array();
        session_destroy();
        if (isset($_COOKIE['user_id'])) {
            unset($_COOKIE['user_id']);
        }
        if (isset($_SESSION["user_id"])) {
            echo "ERROR: Cannot terminate session!";
        } else {
            parent::redirect("index");
        }
    }

}

?>
