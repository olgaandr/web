<?php

class ProfileController extends Controller {

    function index() {

        if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 2) {
            echo $this->view->render('teacher_profile.php');
        } else {
            
            echo $this->view->render('profile.php', array("user"=>$user));
        }
    }

}

?>
