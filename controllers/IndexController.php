<?php

class IndexController extends Controller {

    public $message = "";
    public $password_error = "";
    public $email_error = "";
    public $nothing = "";

    function index() {
        require_once 'config/config.php';
        //session_start();

        if (isSet($_SESSION['user_id'])) {
            $this->redirect("profile");
        }

        //$finalMessageArray= $this->getMessages();
        //'message' => $finalMessageArray, 
        //array('nothing' => $this->nothing,'password_error' => $this->password_error, 'email_error' => $this->email_error)
        echo $this->view->render('index.php');
    }
    
    

    function doLogin() {
        $userModel = new UserModel();
        if (isset($_POST['submit'])) {
            $password = sha1($_POST['pass'] . '123');
            $user = $userModel->getUserIfExist($_POST['email'], $password);
            if (!is_null($user)) {
                $_SESSION['user_id'] = $user->getId();

                if (isset($_POST['autologin'])) {
                    setcookie($cookie_name, 'usr=' . $user->getEmail() . '&hash=' . $password, time() + $cookie_time);
                }
                $_SESSION['user_type'] = $user->getType();
                echo 'if';
                $this->redirect("profile");
            } else {
                $this->addMessage("login", "Email or password is not correct");
                //echo 'else';
                //parent::redirect("index","#log");
                $this->redirect("index","#log");
            }
        }
    }

    function doRegistration() {
        if (isSet($_POST['register'])) {

            //include 'databaseConnect.php';
            $userModel = new UserModel();
            $post_name = mysql_real_escape_string($_POST["name"]);
            $post_surname = mysql_real_escape_string($_POST["surname"]);
            $post_email = mysql_real_escape_string($_POST["new_email"]);

            if (strlen($post_name) == 0 && strlen($post_surname) == 0 && strlen($_POST["pass"]) == 0
                    && strlen($_POST["pass_again"] == 0) && strlen($post_email) == 0) {
                $this->addMessage("register", "Form is empty. ");
                $this->redirect("index", "#reg");
                //$nothing = "You must fill form. ";
                //echo $nothing;
            } else {
                $password_error = "";
                $email_error = "";
                if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9._-]+)+$/", $post_email)) {
                    $email_error = "Email is not valid. ";
                }
                if (strlen($_POST["pass"]) >= 5) {
                    if ($_POST["pass"] != $_POST["pass_again"]) {
                        $password_error = "Passwords are different. ";
                    }
                } else {
                    $password_error = "Password must have at least 5 characters. ";
                }
                //$password_error = checkPass($_POST["pass"]);
                //$email_error = checkEmail($post_email);
                $hashpass = sha1($_POST['pass'] . "123");

                if (strlen($password_error) == 0 && strlen($email_error) == 0
                        && strlen($post_name) != 0 && strlen($post_surname) != 0) {
                    $userModel->registerUser($post_name, $post_surname, $post_email, $hashpass, $_POST["class"], $_POST["user_type"]);
                    //echo '';
                } else {
                    $this->addMessage("register", $password_error);
                    $this->addMessage("register", $email_error);
                    $this->redirect("index", "#reg");
                    //echo $password_error . $email_error . "Something is not filled. ";
                }
            }
            //parent::redirect("index");
        }
    }

    function checkEmail($email) {
        if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9._-]+)+$/", $email)) {
            return "Email is not valid. ";
        }
        return "";
    }

    function checkPass($pass) {
        if (strlen($pass) >= 5) {
            if ($pass != $_POST["pass_again"]) {
                return "Passwords are different. ";
            }
            return "";
        } else {
            return "Password must have at least 5 characters. ";
        }
    }

}

?>
