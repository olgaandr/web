<?php

class UserModel extends Model{

    function getUserIfExist($email, $password) {

        //require_once 'config/config.php';
        //require 'models/databaseObjects/User.php';
        $email = htmlspecialchars($email);
        
        $student=$this->getStudent($email, $password);
        if ($student == NULL) {
            return $this->getTeacher($email, $password);
        }
        else{
            return $student;
        }
        /* if ($sql = $mysqli->prepare('SELECT s.email, s.password AS pass, s.id AS id FROM student AS s WHERE s.email=? AND s.password=?')) {
          $sql->bind_param("ss", $email, $password);
          $sql->execute();
          $result = $sql->get_result();
          } else {
          echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
          }
          if ( $result->num_rows == 0) {

          if ($sql2 = $mysqli->prepare('SELECT t.email, t.password AS pass, t.id AS id FROM teacher AS t WHERE t.email=? AND t.password=?')) {
          $sql2->bind_param("ss", $email, $password);
          $sql2->execute();
          $result = $sql2->get_result();
          } else {
          echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
          }
          if ( $result->num_rows == 0) {
          return NULL;
          } else {
          $row = $result->fetch_row();
          $user = new User($row[2], $email, $password);
          return $user;
          }
          } else {
          $row = $result->fetch_row();
          $user = new User($row[2], $email, $password);
          return $user;
          } */
    }

    function registerUser($name, $surname, $email, $hashpass, $class, $user_type) {
        include 'config/databaseConnection.php';
        //require 'models/databaseObjects/User.php';

        //if ($_POST['user_type'] == 0) {
        if ($user_type == 0) {
            $this->insertStudent($name, $surname, $email, $hashpass, $class);
        } else {
            $this->insertTeacher($name, $surname, $email, $hashpass);
        }
    }

    private function insertStudent($name, $surname, $email, $hashpass, $class) {
        if ($stmt = $this->mysqli->prepare("INSERT INTO  `homeworks`.`student` 
              (`id`,`name`,`email`,`password`,`coins`,`level`,`homeworks_to_next_level`,`class_id`)
              VALUES (NULL,?,?,?,'0','1','5',?)")) {
            $fullname = $name . " " . $surname;
            $stmt->bind_param("ssss", $fullname , $email, $hashpass, $class);
            $stmt->execute();
            echo $name . $surname . $email . $hashpass . " class: " . $class;
        } else {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }
    }

    private function insertTeacher($name, $surname, $email, $hashpass) {
        if ($stmt = $this->mysqli->prepare('INSERT INTO  `homeworks`.`teacher` (`id`,`name`,`email`,`password`)
              VALUES ("","? ?","?","?")')) {
            $stmt->bind_param('ssss', $name, $surname, $email, $hashpass);
        } else {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }
    }

    function getStudent($email, $password) {
        //include 'config/databaseConnection.php';
        require 'models/databaseObjects/User.php';
        if ($sql = $this->mysqli->prepare('SELECT s.email, s.password AS pass, s.id AS id FROM student AS s WHERE s.email=? AND s.password=?')) {
            $sql->bind_param("ss", $email, $password);
            $sql->execute();
            $result = $sql->get_result();
        } else {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }
        if ($result->num_rows == 0) {
            return NULL;
            //$this->getTeacher();
        } else {
            $row = $result->fetch_row();
            $user = new User($row[2], $email, $password,1);
            return $user;
        }
    }

    function getTeacher($email, $password) {
        //include 'config/databaseConnection.php';
        //require 'models/databaseObjects/User.php';
        if ($sql2 = $this->mysqli->prepare('SELECT t.email, t.password AS pass, t.id AS id FROM teacher AS t WHERE t.email=? AND t.password=?')) {
            $sql2->bind_param("ss", $email, $password);
            $sql2->execute();
            $result = $sql2->get_result();
        } else {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }
        if ($result->num_rows == 0) {
            return NULL;
        } else {
            $row = $result->fetch_row();
            $user = new User($row[2], $email, $password,2);
            return $user;
        }
    }

}

?>
