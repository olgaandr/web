<?php

class User {

    private $id;
    private $email;
    private $password;
    private $type;
    
    function __construct($id,$email,$password,$type) {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->type = $type;
    }
    function getId(){
        return $this->id;
    }
    function getPassword(){
        return $this->password;
    }
    function getEmail(){
        return $this->email;
    }
    function getType(){
        return $this->type;
    }
}
?>
