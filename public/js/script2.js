$(document).ready(function(){
    $(".small_content").hide();
    $(".login").show();
    validateForm();
    
    var form = $('#reg_form');
    form.on('submit', function(e){
        e.preventDefault();
        //alert("neco");
        $.ajax( {
            type: "POST",
            url: form.attr( 'action' ),
            data: form.serialize(),
            success: function( data ){
                    
                //alert(data);
                if(data==""){
                    form[0].reset();
                    $("#login_error").text("Now you can login into your account");
                    $(".small_content").hide();
                    $(".login").show();
                    menuChange($(".click:first"));
                }
                else{
                    $("#error").text(data);
                }
            //console.log( response );
            }
        } );
    } );
    /*var form = $('#login_form');
                form.on('submit', function(e){
                    e.preventDefault();
                    $.ajax( {
                        type: "POST",
                        url: form.attr( 'action' ),
                        data: form.serialize(),
                        success: function( data ) {
                            //alert(data);
                            $("#login_error").text(data);
                            //console.log( response );
                        }
                    } );
                });*/
    $(".register input").focus(function() {
        $(".reg_error").hide();
    });
    
    $(".reg_submit").click(function() {
        $(".reg_error").show();
        
    //})
    });
         
    $(".click").click(function(){
        menuChange(this);
        /*$(".click").css('backgroundColor', '#FF7F00');
        $(this).css('backgroundColor', 'white');
        $(".small_content").hide();
        var show = "." + ($(this).attr('id'));
        $(show).show();*/
                    
        
    });
    $("#checkbox").click(function(){
        //alert($('#checkbox').attr("checked"));
    });
    
});

function menuChange(item){
    //$(".click").css('backgroundColor', '#FF7F00');
    $(".click").css('backgroundColor', 'white');
    //$(this).css('backgroundColor', 'white');
    //$(item).css('backgroundColor', 'white');
    $(item).css('backgroundColor', '#E6E6E6');
    $(".small_content").hide();
    //var show = "#" + this.id;
    var show = "." + ($(item).attr('id'));
    $(show).show();
}

function validateForm(){
    $("#rReservation").change(function(){
        if ($("#rReservation").val() == "1") {
            $("#class").hide();
        } else {
            $("#class").show();
        //$('.mRequired').removeClass('required');
        }
    });
    $("#pass_again").keyup(function() {
        if($("#pass").val()!=$("#pass_again").val()){
            $("#error").text("Passwords are different ")
        }
        else{
            $("#error").text("")
        }
    });
    $("#pass").keyup(function() {
        if($("#pass").val()!="" && $("#pass").val().length<=5){
            //alert($("#pass").val());
            $("#error").text("Password is too short ")
        }
        else{
            $("#error").text("")
        }
    });
    $("#new_email").keyup(function() {
        //alert(emailReg.test(emailaddressVal));
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var emailVal = $("#new_email").val();
        if(!emailReg.test(emailVal)){
            $("#error").text("Email is not valid");
        }
        else{
            $("#error").text("")
        }
    });
}