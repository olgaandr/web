$(document).ready(function() {
    var path = window.location.hash;
    if(path == "#log"){
        logClick();
    }
    if(path == "#reg"){
        regClick();
    }
    $(".lognav-log").click(function() {
        logClick();
    });
    $(".lognav-reg").click(function() {
        regClick();
    });
    
    /*$("#userType").click(function() {
        $("#class").hide();
    });*/
    
    validateForm();
    
});
function logClick(){
    $(".lognav-log").css("background-color", "#FFFFFF");
    $(".lognav-log div").css("color", "#5C5C5C");
    $(".lognav-reg").css("background-color", "#5C5C5C");
    $(".lognav-reg div").css("color", "#FFFFFF");
    $(".insidelogin").delay(400).fadeIn(400);
    $(".insidelogin2").fadeOut(400);
    $(".login").animate({height: "400px"});
    window.location.replace(window.location.pathname + '#log');
}
function regClick(){
    $(".lognav-reg").css("background-color", "#FFFFFF");
    $(".lognav-reg div").css("color", "#5C5C5C");
    $(".lognav-log").css("background-color", "#5C5C5C");
    $(".lognav-log div").css("color", "#FFFFFF");
    $(".insidelogin2").delay(400).fadeIn(400);
    $(".insidelogin").fadeOut(400);
    $(".login").animate({height: "600px"});
    window.location.replace(window.location.pathname + '#reg');
}
function validateForm(){
    $("#userType").change(function(){
        if ($("#userType").val() == "1") {
            $(".li6").hide("slow");
        } else {
            $(".li6").show("slow");
        //$('.mRequired').removeClass('required');
        }
    });
    $("#pass_again").keyup(function() {
        if($("#pass").val()!=$("#pass_again").val()){
            $("#error").text("Passwords are different ")
        }
        else{
            $("#error").text("")
        }
    });
    $("#pass").keyup(function() {
        if($("#pass").val()!="" && $("#pass").val().length<=5){
            //alert($("#pass").val());
            $("#error").text("Password is too short ")
        }
        else{
            $("#error").text("")
        }
    });
    $("#new_email").keyup(function() {
        //alert(emailReg.test(emailaddressVal));
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var emailVal = $("#new_email").val();
        if(!emailReg.test(emailVal)){
            $("#error").text("Email is not valid");
        }
        else{
            $("#error").text("")
        }
    });
}