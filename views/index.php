<?php ?>
<html>
    <head>
        <title></title>
        <script src="public/js/jquery-1.7.1.min.js"></script>
        <script src="public/js/script.js"></script>
        <link href="public/css/style.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body>
        <div class="container">
            <div class="nav">
                <div class="login">
                    <div class="lognav">
                        <div class="lognav-log">
                            <div align="center">Login</div>
                        </div>
                        <div class="lognav-reg">
                            <div align="center">Register</div>
                        </div>
                    </div>
                    <div class="insidelogin">
                        <div class="title" align="center">
                            <p>You already have an account! Login here</p>
                        </div>
                        <?php //if (isset($message)) {echo $message;}  ?>
                        <?php foreach ($this->messages as $m): ?>
                        <?php if($m->type=="login"): ?>
                                    <div class="error_message" align="center"><?= $m->text ?></div>
                                <?php endif; ?>
                        <?php endforeach; ?>
                        <div class="divinput">
                            <form method="post" action="index/doLogin">
                                <ul>
                                    <li>
                                        <div class="liinsideul">
                                            <div class="inputtext"><div><img src="public/images/email.png"/></div></div>
                                            <div  class="inputdiv"><input type="email" placeholder="Email" name="email"/></div>
                                        </div>
                                    </li>
                                    <li class="li2">
                                        <div class="liinsideul">
                                            <div class="inputtext"><div align="center"><img src="public/images/password.png"/></div></div>
                                            <div  class="inputdiv"><input type="password" placeholder="Password" name="pass"/></div>
                                        </div>
                                    </li>
                                </ul>

                                <div class="button">
                                    <input type="submit" value="LOGIN" name="submit"/>

                                </div>
                            </form>
                        </div>


                    </div>
                    <div class="insidelogin2">
                        <div class="title" align="center">
                            <p>You don't have an account! Register here</p>
                        </div>
                        <div id="php_error"><? //$nothing.$email_error.$password_error  ?>
                            <?php foreach ($this->messages as $m): ?>
                        <?php if($m->type=="register"): ?>
                                    <div class="error_message" align="center"><?= $m->text ?></div>
                                <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                        <div class="divinput">
                            <form method="post" action="index/doRegistration">
                                <ul>
                                    <li class="7">
                                        <div class="liinsideul">
                                            <div class="inputtext"></div>
                                            <div  class="inputdiv"><input id="name" type="text" placeholder="Name" name="name"/></div>
                                        </div>
                                    </li>
                                    <li class="li8">
                                        <div class="liinsideul">
                                            <div class="inputtext"></div>
                                            <div  class="inputdiv"><input id="surname" type="text" placeholder="Surname" name="surname"/></div>
                                        </div>
                                    </li>
                                    <li class="li1">
                                        <div class="liinsideul">
                                            <div class="inputtext"><div><img src="public/images/email.png"/></div></div>
                                            <div  class="inputdiv"><input id="new_email" type="email" placeholder="Email" name="new_email"/></div>
                                        </div>
                                    </li>
                                    <li class="li2">
                                        <div class="liinsideul">
                                            <div class="inputtext"><div align="center"><img src="public/images/password.png"/></div></div>
                                            <div  class="inputdiv"><input id="pass" type="password" placeholder="Password" name="pass"/></div>
                                        </div>
                                    </li>
                                    <li class="li4">
                                        <div class="liinsideul">
                                            <div class="inputtext"><div align="center"><img src="public/images/password.png"/></div></div>
                                            <div  class="inputdiv"><input id="pass_again" type="password" placeholder="Repeat Password" name="pass_again"/></div>
                                        </div>
                                    </li>
                                    <li class="li5">
                                        <div class="liinsideul">
                                            <div class="inputtext"><div align="center"></div></div>
                                            <select id="userType" name="user_type">
                                                <option value="0">Student</option>
                                                <option value="1">Teacher</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="li6">
                                        <div class="liinsideul">
                                            <div class="inputtext"><div align="center"></div></div>
                                            <select id="class" name="class" class="">
                                                <option value="1">C1</option>
                                                <option value="2">C2</option>
                                                <option value="3">C3</option>
                                                <option value="4">C4</option>
                                                <option value="5">G1</option>
                                                <option value="6">G2</option>
                                                <option value="7">G3</option>
                                                <option value="8">G4</option>
                                            </select>
                                        </div>
                                    </li>
                                </ul><div class="button2">
                                    <a href="#"><input type="submit" value="Register" name="register"/></a>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
