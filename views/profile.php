<html>
    <head>
        <title></title>
        <link href="public/css/style.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body>
        <div id="menu">
            <div class="content-menu">
                <div id="logo">
                    <a href="#">Web page</a>
                </div>
                <div class="item"><a href="">My profile</a></div>
                <!--<div class="item"><h1><a href="register.html">Homeworks</a></h1></div>!-->
                <div class="item"><a href="improvements">Improvements</a></div>
                <div class="item"><a href="achievements">Achievements</a></div>
                <div class="item"><a href="ranking">Ranking</a></div>
                <div class="item"><a href="logout">Logout</a></div>
            </div>
        </div>
        <div id="content">
            <div class="part">
                <div class="title">
                    <h2>My profile</h2>
                </div>
                <div>
                    <div class="user">
                        <div class='leveluser'>
                            <img src="public/images/levels/level25.png" id="levelImg">
                            <br/>
                            <span class="byline name">Name Surname</span>
                        </div>
                        <div class="table">
                            <p>Homeworks to next level: <span>20</span></p>
                            <p>Complete homeworks: <span>20</span></p>
                            <p>Incomplete homeworks: <span>20</span></p>
                            <p>Coins: <span>50</span></p>
                        </div>
                    </div>

                </div>

            </div>
            <div class="part">
                <div class="title">
                    <h2>Subjects</h2>
                </div>
                <div class='subjectcontent'>
                    <div class="subjectInfo">
                        <h1><a href="subject_detail.html" class="subjectTitle">Mathematic</a></h1>
                        <div class="score">
                            <div class="actualScore" style="width: 300px"></div>
                        </div>
                        <h1 class="precents">80%</h1>
                    </div>
                    <div class="subjectInfo">
                        <h1><a href="subject_detail.html" class="subjectTitle">French</a></h1>
                        <div class="score">
                            <div class="actualScore" style="width: 300px"></div>
                        </div>
                        <h1 class="precents">80%</h1>
                    </div>
                    <div class="subjectInfo">
                        <h1><a href="subject_detail.html" class="subjectTitle">Czech</a></h1>
                        <div class="score">
                            <div class="actualScore" style="width: 300px"></div>
                        </div>
                        <h1 class="precents">80%</h1>
                    </div>
                    <div class="subjectInfo">
                        <h1><a href="subject_detail.html" class="subjectTitle">History</a></h1>
                        <div class="score">
                            <div class="actualScore" style="width: 300px"></div>
                        </div>
                        <h1 class="precents">80%</h1>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
