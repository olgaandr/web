<?php

class View {

    private $messages;

    public function __construct($messages) {
        $this->messages = $messages;
    }

    public function render($file, $variables = array()) {
        extract($variables);

        ob_start();
        //echo 'Mvc2/views/'.$file;
        include 'views/' . $file;
        $renderedView = ob_get_clean();

        return $renderedView;
    }

}

?>
