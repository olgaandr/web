<?php

class Controller {

    public function __construct() {
        session_start();
        $this->view = new View($this->getMessages());
    }

    function redirect($controller, $function = NULL) {
        if (isset($function)) {
            //header("Location: /Mvc2/$controller$function");
            //echo strpos($function,'#');
            if(strpos($function,'#') !== FALSE){
                header("Location: /Mvc2/$controller$function");
            }
            else{
                header("Location: /Mvc2/$controller/$function");
            }
            
        } else {
            header("Location: /Mvc2/$controller");
        }
        exit;
    }

    public function addMessage($type, $text) {
        $m = new Message($type, $text);
        $message = $m->serialize();
        //session_start();
        if(isset($_SESSION['message'])){
            $_SESSION['message'] = $_SESSION['message'] . $message;
        }
        else{
            $_SESSION['message'] = $message;
        }
        
        //dostanu objekt message, ma serialize, ulozim do session
    }
    
    public function getMessages() {
        $finalMessageArray = array();
        if (isSet($_SESSION['message'])) {
            $m = explode(';', $_SESSION['message']);
            $m = array_diff($m, array(''));
            foreach ($m as $mes) {
                $myMes = explode(',', $mes);
                $finalMessageArray[] = new Message($myMes[0],$myMes[1]);
            }
            unset($_SESSION['message']);
        }
        
        return $finalMessageArray;
    }

}

?>
