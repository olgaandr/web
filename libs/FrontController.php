<?php

class FrontController {

    function __construct() {
        $url = $_GET['url'];
        //$url = rtrim($url, '/');
        $url = explode('/', $url);
        $url = array_diff($url, array(''));

        if (!isset($url[0])) {
            header("Location: /Mvc2/index");
        }
        
        /*foreach ($url as $value) {
            if(strpos($value,'#')){
                $value = str_replace('#', '', $value);
            }
        }*/
        
        $controllerName = ucfirst($url[0]) . 'Controller';

        $file = 'controllers/' . ucfirst($url[0]) . 'Controller.php';

        if (!file_exists($file)) {
            $controller = new errorController();
            return false;
        }

        $controller = new $controllerName();
        if (isset($url[2])) {
            $controller->{$url[1]}($url[2]);
        } else {
            if (isset($url[1])) {
                $controller->{$url[1]}();
            } else{
                $controller ->index();
            }
        }
    }

}

?>
