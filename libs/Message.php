<?php

class Message {

    public $type;
    public $text;

    function __construct($type, $text) {
        $this->type = $type;
        $this->text = $text;
    }

    public function serialize() {
        $val=$this->type.",".$this->text.";";
        return $val;
    }
}

?>
